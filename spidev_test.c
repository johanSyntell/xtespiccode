/*
 * SPI Interrupt code for XTE transmitting over UDP
 * Compiled using the cross arm gcc compiler
 * Author	: 	Johan de Villiers
 * Date 	:	26 Feb 2021
 * Version	:	1	-	First release of the code
 * Version  :  	1.1 - 	Fixed Timing
 * Version 	:	1.2 - 	Update the comm packet and reading all detector statuses, also included a message counter
 * Version 	:	1.3 - 	Add filtering to the spi packet and change the clock to show seconds aswell
 * 
 */
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include <byteswap.h>


//#defines 
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define BUFLEN 512	//Max length of buffer
#define PORT   8888//The port on which to send data
#define DETECTOR_UPDATE 0
#define TEN_SEC_UPDATE 	1

//Global variables
static char *xte_ip = "127.0.0.1";
static const char *device = "/dev/spidev1.0";
static uint8_t mode = 3;
static uint8_t bits = 8;
static uint32_t speed = 10000000;
static uint16_t delay = 0;
int fd;
struct sockaddr_in si_other;
int si;
int slen=sizeof(si_other);
uint32_t lastDetectorStatus = 0;
uint32_t tenSecondCounter = 0;
uint32_t messageCounter = 0;


//Function definitions
void detectorMonitor (int signum);
void errorHandler (char *handler);
static void processAbort(const char *pAbort);
static void transfer(int fd);
static void spiControl(const char *prog);
static void spiOptions(int argc, char *argv[]);
int udpInit();
int spiInit();
int timerInit();
uint32_t retrieveTime();
uint32_t spiMessage();
void udpTransmitBuffer (uint32_t messageType, uint32_t messageCounter, uint32_t detectorStatus, uint32_t timeValue);


int main(int argc, char *argv[])
{
	int ret = 0;
	spiOptions(argc, argv);
	
	ret = udpInit();
	ret |= spiInit();
	ret |= timerInit();
	
	if (ret == -1){
		printf("error\n");
		return 0;
	}
	return 1;
}


//////////////////////////////////////////////////////////////////////////////////
///////////////////////////UDP Init Code//////////////////////////////////////////
int udpInit(){

	int ret = 0;
	
	ret = (si=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP));
	if ( ret == -1){
		
		errorHandler("socket");
	} else {
		
		printf("\nUDP Socket Opened\n");
	}
	
	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	
	ret = inet_aton(xte_ip , &si_other.sin_addr);
	if (ret == 0) {
		
		fprintf(stderr, "inet_aton() failed\n");
		return ret = -1;
	} else {
		
		printf("UDP Server IP %s\n\n", xte_ip);
		return ret;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////SPI Init Code////////////////////////////////////////////////////////
int spiInit(){
	
	int ret = 0;
	fd = open(device, O_RDWR);
	if (fd < 0){

		processAbort("can't open device");
	}
	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1){

		processAbort("can't set spi mode");
	}
	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1){
		
		processAbort("can't get spi mode");
	}
	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1){
		
		processAbort("can't set bits per word");
	}
	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1){
		
		processAbort("can't get bits per word");
	}
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1){
		
		processAbort("can't set max speed hz");
	}
	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1){
		
		processAbort("can't get max speed hz");
	}
	
	if (ret == -1){
		
		return ret;
	} else {
		
		printf("SPI port open\n");
		printf("spi mode: %d\n", mode);
		printf("bits per word: %d\n", bits);
		printf("max speed: %d Hz (%d MHz)\n", speed, speed/1000000);
		return ret;
	}	
}

//////////////////////////////////////////////////////////////////////////////
/////////////////////////Timer Init Code//////////////////////////////////////
int timerInit(){
	
	timer_t timer1;
	timer_t timer2;
	struct itimerspec newValueTimer1;
	struct itimerspec newValueTimer2;
	struct itimerspec oldValue;
	struct sigaction action;
 	struct sigevent sevent;
   	sigset_t set;
    int signum;
	int ret = 0;
	
	// SIGALRM for printing time 
	memset (&action, 0, sizeof (struct sigaction));
	action.sa_handler = detectorMonitor;
	ret = sigaction (SIGALRM, &action, NULL);
	if (ret == -1){
		perror ("sigaction");
	}

	// for program completion 
	memset (&sevent, 0, sizeof (struct sigevent));
	sevent.sigev_notify = SIGEV_SIGNAL;
	sevent.sigev_signo = SIGRTMIN;
	ret = timer_create (CLOCK_MONOTONIC, NULL, &timer1);
	if (ret == -1){
		perror ("timer_create");
	}

	//Timer 1 settings
	//Timer 1 setup to run every 1ms
   	newValueTimer1.it_interval.tv_sec = 0;
    newValueTimer1.it_interval.tv_nsec = 1000000;  // 1ms 
   	newValueTimer1.it_value.tv_sec = 0;
	newValueTimer1.it_value.tv_nsec = 1000000;     // 1ms 
	
	ret = timer_settime (timer1, 0, &newValueTimer1, &oldValue);
	if (ret == -1){

		perror ("timer_settime");
	}
	ret = sigemptyset (&set);
	if (ret == -1){

		perror ("sigemptyset");
	}
	ret = sigaddset (&set, SIGRTMIN);
	if (ret == -1){

		perror ("sigaddset");
	}
	ret = sigprocmask (SIG_BLOCK, &set, NULL);
	if (ret == -1){
		
		perror ("sigprocmask");
	}
	ret = timer_create (CLOCK_MONOTONIC, &sevent, &timer2);
	if (ret == -1){
		
		perror ("timer_create");
	}
	
	//Timer 2 settings
	//Timer 2 set to run forever if all variables are set to 0
	newValueTimer2.it_interval.tv_sec = 0;
	newValueTimer2.it_interval.tv_nsec = 0;
	newValueTimer2.it_value.tv_sec = 0;
   	newValueTimer2.it_value.tv_nsec = 0;
	ret = timer_settime (timer2, 0, &newValueTimer2, &oldValue);
	
	if (ret == -1){
		
		perror ("timer_settime");
	}

	// wait for completion signal (30s)  
	ret = sigwait (&set, &signum);
	if (ret == -1){
		
		perror ("sigwait");
	} else {
		
		printf("Done\n");
		close(fd);
	}
	
	return ret;
}

//retriveTime
//Calls system time when event occurs on the detectors
uint32_t retrieveTime(){
	
	int ret = 0;
	int length = 6;
	struct timespec tp;
	uint32_t timeValue = 0;
	ret = clock_gettime (CLOCK_MONOTONIC, &tp);
    if (ret == -1){
        perror ("clock_gettime");
	}
	timeValue = ((tp.tv_sec * 1000) + (tp.tv_nsec / 1000000)) & 0xffffffff;

	return timeValue;
}
//spiMessage
//Request the detector data at fixed intervals (determined by timerInit() function)
//This function also filters the messages to ensure it only responds to valid messages 
uint32_t spiMessage(){

	uint8_t tx[3] = {0x00, 0x00, 0x00};
	uint8_t rx[3] = {0x00, 0x00, 0x00};
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE(tx),
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};

	uint32_t detectorStatus = 0;
	uint32_t data;
	uint8_t addr = 0;
	for (int ii = 0 ; ii < 4; ii++){
		tx[0] = addr >> 4;
		tx[1] = ((addr & 0x0f) << 4) | 0x08;
		int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
		if (ret < 1){
			processAbort("can't send spi message");
		}
		
		while ((rx[0] == 0xff)){// || (rx[1] == 0x0c)||(rx[1] == 0x08)){
			ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
			if (ret < 1){
				processAbort("can't send spi message");
			}
		}
		data = rx[2];
		detectorStatus |= data << (ii * 8);	
		addr += 4;
	}
	return detectorStatus;	
}

/*uint32_t spiMessage(){

	int ret = 0;
	uint8_t tx[3] = {0x00, 0x00, 0x00};
	int ii = 0;
	
	char rx[ARRAY_SIZE(tx)] = {0, };
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE(tx),
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};
	uint32_t detectorStatus = 0;
	for (ii = 0 ; ii < 4; ii++){
		
		ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
		if (ret < 1){
			processAbort("can't send spi message");
		}
		
		while ((rx[0] == 0xff)){// || (rx[1] == 0x0c)||(rx[1] == 0x08)){
			ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
			if (ret < 1){
				processAbort("can't send spi message");
			}
		}
		detectorStatus |= rx[2] << (ii * 8);	
		tx[1] = tx[1] + 4;
	}
	return detectorStatus;	
}*/

//detectorMonitor function runs on the interrupt timer to poll the FPGA for detector data
void detectorMonitor (int signum)
{   
    
    uint32_t timeValue = 0;// [10] = {'\0'};
	int ret;
	uint32_t detectorStatus = 0;
	tenSecondCounter++;
	detectorStatus = spiMessage();
	if (detectorStatus != lastDetectorStatus){
		
		lastDetectorStatus = detectorStatus;	
		timeValue = retrieveTime ();
		udpTransmitBuffer(DETECTOR_UPDATE, messageCounter, detectorStatus, timeValue);	
		messageCounter++;
	}
	if (tenSecondCounter >= 10000){
		
		//detectorStatus = spiMessage();
		timeValue = retrieveTime ();
		udpTransmitBuffer(TEN_SEC_UPDATE, messageCounter, detectorStatus, timeValue);	
		tenSecondCounter = 0;
		messageCounter++;
		
	}
}

//udpTransmitBuffer transmits the buffer information vir UDP 
void udpTransmitBuffer (uint32_t messageType, uint32_t messageCounter, uint32_t detectorStatus, uint32_t timeValue){
	
	uint32_t buffer[4];
	buffer [0] = messageType;
	buffer [1] = messageCounter;
	buffer [2] = detectorStatus;
	buffer [3] = timeValue;
	if (sendto(si, buffer, 16 , 0 , (struct sockaddr *) &si_other, slen)==-1){
		
		errorHandler("sendto()");
	} 
}


void errorHandler(char *handler)
{
	perror(handler);
	exit(1);
}

static void processAbort(const char *pAbort)
{
	perror(pAbort);
}

static void spiControl(const char *prog)
{
	printf("Usage: %s [-DsbdlHOLC3]\n", prog);
	puts("  -D --device   device to use (default /dev/spidev1.1)\n"
	     "  -s --speed    max speed (Hz)\n"
	     "  -d --delay    delay (usec)\n"
	     "  -b --bpw      bits per word \n"
	     "  -l --loop     loopback\n"
	     "  -H --cpha     clock phase\n"
	     "  -O --cpol     clock polarity\n"
	     "  -L --lsb      least significant bit first\n"
	     "  -C --cs-high  chip select active high\n"
	     "  -3 --3wire    SI/SO signals shared\n");
	exit(1);
}

static void spiOptions(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "xte",     1, 0, 'x' },
			{ "device",  1, 0, 'D' },
			{ "speed",   1, 0, 's' },
			{ "delay",   1, 0, 'd' },
			{ "bpw",     1, 0, 'b' },
			{ "loop",    0, 0, 'l' },
			{ "cpha",    0, 0, 'H' },
			{ "cpol",    0, 0, 'O' },
			{ "lsb",     0, 0, 'L' },
			{ "cs-high", 0, 0, 'C' },
			{ "3wire",   0, 0, '3' },
			{ "no-cs",   0, 0, 'N' },
			{ "ready",   0, 0, 'R' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "x:D:s:d:b:lHOLC3NR", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'x':
			xte_ip = optarg;
			break;
		case 'D':
			device = optarg;
			break;
		case 's':
			speed = atoi(optarg);
			break;
		case 'd':
			delay = atoi(optarg);
			break;
		case 'b':
			bits = atoi(optarg);
			break;
		case 'l':
			mode |= SPI_LOOP;
			break;
		case 'H':
			mode |= SPI_CPHA;
			break;
		case 'O':
			mode |= SPI_CPOL;
			break;
		case 'L':
			mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			mode |= SPI_CS_HIGH;
			break;
		case '3':
			mode |= SPI_3WIRE;
			break;
		case 'N':
			mode |= SPI_NO_CS;
			break;
		case 'R':
			mode |= SPI_READY;
			break;
		default:
			spiControl(argv[0]);
			break;
		}
	}
}
